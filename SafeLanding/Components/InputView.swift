//
//  InputView.swift
//  SafeLanding
//
//  Created by Data on 04.07.24.
//

import SwiftUI

struct InputView: View {
    @Binding var text: String
    var tittle: String
    var placeholder: String
    var isSecureField = false
    var body: some View {
        VStack(alignment: .leading, spacing: 15) {
            Text(tittle)
                .bold()
                .font(.footnote)
            
            if isSecureField {
                SecureField(placeholder, text: $text)
                    .font(.system(size: 14))
            } else {
                TextField(placeholder, text: $text)
                    .font(.system(size: 14))
            }
            Divider()
        }
    }
}

#Preview {
    InputView(text: .constant(""), tittle: "Email edres", placeholder: "...@gmail.com")
}
