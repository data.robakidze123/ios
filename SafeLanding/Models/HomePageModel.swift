//
//  HomePageModel.swift
//  SafeLanding
//
//  Created by Data on 05.07.24.
//

import Foundation

struct Business: Codable, Identifiable {
    let id: Int
    let name: String
    let description: String
    let raised: Int
    let goal: Int
    let deadline: String
    let image: String
}
