//
//  DetailsView.swift
//  SafeLanding
//
//  Created by Data on 04.07.24.
//

import SwiftUI

struct DetailsView: View {
    var business: Business
    
    @State private var investmentAmount: String = ""
    @State private var image: Image? = nil
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 20) {
                if let image = image {
                    image
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(height: 300)
                        .clipped()
                } else {
                    Image(systemName: "photo")
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(height: 300)
                        .clipped()
                        .cornerRadius(10)
                        .shadow(radius: 5)
                        .foregroundColor(.gray)
                }
                
                Text(business.name)
                    .bold()
                    .font(.title)
                    .padding()
                HStack {
                    Text(business.description)
                        .bold()
                    Spacer()
                }
                .padding()
                
                HStack {
                    Text("$\(business.raised) raised of $\(business.goal)")
                        .font(.caption)
                        .foregroundColor(.gray)
                    Spacer()
                    Text("Deadline: \(business.deadline)")
                        .font(.caption)
                        .foregroundColor(.gray)
                }
                .padding([.leading, .trailing, .top])
                
                ProgressView(value: Double(business.raised) / Double(business.goal))
                    .padding(.horizontal)
                
                TextField("Enter how much you want to invest", text: $investmentAmount)
                    .padding()
                    .background(Color(UIColor.systemGray6))
                    .keyboardType(.numberPad)
                    .cornerRadius(8)
                    .padding()
                
                Button(action: {
                    print("Invest button tapped with amount: \(investmentAmount)")
                }) {
                    Text("INVEST")
                        .font(.headline)
                        .frame(maxWidth: .infinity)
                        .padding()
                        .background(Color.blue)
                        .foregroundColor(.white)
                        .cornerRadius(8)
                }
                .padding()
                
                Spacer()
            }
            .padding(.top, 20)
        }
        .edgesIgnoringSafeArea(.top)
        .onAppear {
            loadImage()
        }
    }
    private func loadImage() {
        guard let url = URL(string: business.image) else { return }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let data = data, let uiImage = UIImage(data: data) {
                DispatchQueue.main.async {
                    self.image = Image(uiImage: uiImage)
                }
            }
        }.resume()
    }
}

#Preview {
    DetailsView(business: Business(id: 1, name: "Doe Enterprises", description: "Innovative Solutions", raised: 10000, goal: 50000, deadline: "2025-01-01", image: "https://example.com/image.jpg"))
}
