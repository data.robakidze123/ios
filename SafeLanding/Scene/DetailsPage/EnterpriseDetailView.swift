//
//  EnterpriseDetailView.swift
//  SafeLanding
//
//  Created by Data on 05.07.24.
//

//import SwiftUI
//
//struct EnterpriseDetailView: View {
//    var business: Business
//    
//    @State private var investmentAmount: String = ""
//    @State private var image: Image? = nil
//    
//    var body: some View {
//        ScrollView {
//            VStack(alignment: .leading, spacing: 20) {
//                if let image = image {
//                    image
//                        .resizable()
//                        .aspectRatio(contentMode: .fill)
//                        .frame(height: 300)
//                        .clipped()
//                } else {
//                    Image(systemName: "photo")
//                        .resizable()
//                        .aspectRatio(contentMode: .fill)
//                        .frame(height: 300)
//                        .clipped()
//                        .cornerRadius(10)
//                        .shadow(radius: 5)
//                        .foregroundColor(.gray)
//                }
//                
//                Text(business.name)
//                    .bold()
//                    .font(.title)
//                    .padding()
//                HStack {
//                    Text(business.description)
//                        .bold()
//                    Spacer()
//                }
//                .padding()
//                
//                HStack {
//                    Text("$\(business.raised) raised of $\(business.goal)")
//                        .font(.caption)
//                        .foregroundColor(.gray)
//                    Spacer()
//                    Text("Deadline: \(business.deadline)")
//                        .font(.caption)
//                        .foregroundColor(.gray)
//                }
//                .padding([.leading, .trailing, .top])
//                
//                ProgressView(value: Double(business.raised) / Double(business.goal))
//                    .padding(.horizontal)
//                
//                TextField("Enter how much you want to invest", text: $investmentAmount)
//                    .padding()
//                    .background(Color(UIColor.systemGray6))
//                    .keyboardType(.numberPad)
//                    .cornerRadius(8)
//                    .padding()
//                
//                Button(action: {
//                    print("Invest button tapped with amount: \(investmentAmount)")
//                }) {
//                    Text("INVEST")
//                        .font(.headline)
//                        .frame(maxWidth: .infinity)
//                        .padding()
//                        .background(Color.blue)
//                        .foregroundColor(.white)
//                        .cornerRadius(8)
//                }
//                .padding()
//                
//                Spacer()
//            }
//            .padding(.top, 20)
//        }
//        .edgesIgnoringSafeArea(.top)
//        .onAppear {
//            loadImage()
//        }
//    }
//    
//    private func loadImage() {
//        guard let url = URL(string: business.image) else { return }
//        
//        URLSession.shared.dataTask(with: url) { data, response, error in
//            if let data = data, let uiImage = UIImage(data: data) {
//                DispatchQueue.main.async {
//                    self.image = Image(uiImage: uiImage)
//                }
//            }
//        }.resume()
//    }
//}



//struct EnterpriseDetailView2: View {
//    @State private var investmentAmount: String = ""
//    
//    var body: some View {
//        VStack(alignment: .leading) {
//            Image(.projectPhoto)
//                .resizable()
//                .aspectRatio(contentMode: .fill)
//                .frame(height: 300)
//                .clipped()
//            Text("Doe Enterprises")
//                .bold()
//                .font(.title)
//                .padding()
//            HStack {
//                Text("We provide innovative tech solutions.")
//                    .bold()
//                Spacer()
//            }
//            .padding()
//            
//            HStack {
//                Text("$10000 raised of $50000")
//                    .font(.caption)
//                    .foregroundColor(.gray)
//                Spacer()
//                Text("Deadline: 1/1/2025")
//                    .font(.caption)
//                    .foregroundColor(.gray)
//            }
//            .padding([.leading, .trailing, .top])
//            
//            ProgressView(value: 0.2)
//                .padding([.leading, .trailing])
//            TextField("Enter how much you want to invest", text: $investmentAmount)
//                .padding()
//                .background(Color(UIColor.systemGray6))
//                .keyboardType(.numberPad)
//                .cornerRadius(8)
//                .padding()
//            
//            Button {
//                print("gadairicxa")
//            } label: {
//                Text("INVEST")
//                    .font(.headline)
//                    .frame(maxWidth: .infinity)
//                    .padding()
//                    .background(Color.blue)
//                    .foregroundColor(.white)
//                    .cornerRadius(8)
//            }
//            .padding()
//        }
//    }
//}
