//
//  ContentView.swift
//  SafeLanding
//
//  Created by Data on 03.07.24.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView {
            HomePage()
                .tabItem {
                    Label("Home", systemImage: "house")
                }
            
            ProfileView()
                .tabItem {
                    Label("Profile", systemImage: "person")
                }
            
            SettingView()
                .tabItem {
                    Label("Setting", systemImage: "gear")
                }
        }
        .accentColor(Color.blue)
    }
}

#Preview {
    ContentView()
}
