//
//  RegistrationView.swift
//  SafeLanding
//
//  Created by Data on 04.07.24.
//

import SwiftUI

struct RegistrationView: View {
    @State private var email = ""
    @State private var fullName = ""
    @State private var password = ""
    @State private var confirmPassword = ""
    @State private var mobileNumber = ""
    @State private var isMale = true
    
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        VStack {
            Image(.logo)
                .resizable()
                .foregroundColor(.blue)
                .scaledToFit()
                .frame(width: 100, height: 90)
        }
        
        VStack(spacing: 24) {
            InputView(text: $email,
                      tittle: "Email address",
                      placeholder: "Enter your email")
            .autocapitalization(.none)
            
            InputView(text: $fullName,
                      tittle: "Full name",
                      placeholder: "Enter your name")
            
            InputView(text: $password,
                      tittle: "Password",
                      placeholder: "Enter your password",
                      isSecureField: true)
            
//            InputView(text: $confirmPassword,
//                      tittle: "Confirm Password",
//                      plaseholder: "Confirm password",
//                      isSecureField: true)
            
            HStack {
                InputView(text: $mobileNumber,
                          tittle: "Phone",
                          placeholder: "Enter your phone number")
                .keyboardType(.numberPad)
                
                Picker("Gender", selection: $isMale) {
                    Text("Male").tag(true)
                    Text("Female").tag(false)
                }
                .pickerStyle(SegmentedPickerStyle())
                .padding(.horizontal)
            }
        }
        .padding(.horizontal)
        .padding(.top, 12)
        
        Button(action: {
            // Implement sign-up logic
            print("User is signing up")
        }, label: {
            HStack {
                Text("SIGN UP")
                    .bold()
                Image(systemName: "arrow.right")
            }
            .foregroundColor(.white)
            .frame(maxWidth: .infinity, minHeight: 50)
            .background(Color.blue)
            .cornerRadius(10)
        })
        .padding(.horizontal, 16)
        .padding(.top, 20)
        
        Spacer()
        
        Button(action: {
            dismiss()
        }) {
            HStack(spacing: 3) {
                Text("Already have an account?")
                Text("Sign in")
                    .bold()
            }
            .font(.system(size: 15))
        }
    }
}

#Preview {
    RegistrationView()
}

