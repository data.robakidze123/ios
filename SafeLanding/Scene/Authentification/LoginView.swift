//
//  LoginView.swift
//  SafeLanding
//
//  Created by Data on 04.07.24.
//

import SwiftUI

struct LoginView: View {
    @State private var email = ""
    @State private var password = ""
    var body: some View {
        NavigationStack {
            // Image
            Image(.logo)
                .resizable()
                .foregroundStyle(.blue)
                .scaledToFill()
                .frame(width: 100, height: 120)
            
            // TextFields
            VStack(spacing: 24) {
                InputView(text: $email,
                          tittle: "Email address",
                          placeholder: "Enter your email")
                .autocapitalization(.none)
                
                InputView(text: $password,
                          tittle: "password",
                          placeholder: "Enter your password",
                          isSecureField: true)
            }
            .padding(.horizontal)
            .padding(.top, 12)
            
            // Sing in button
            
//            Button(action: {
//                print("User is log in")
//            }, label: {
//                HStack {
//                    Text("SIGN IN")
//                        .bold()
//                    Image(systemName: "arrow.right")
//                }
//                .foregroundStyle(.white)
//                .frame(width: UIScreen.main.bounds.width - 32, height: 50)
//            })
//            .background(Color.blue)
//            .cornerRadius(10)
//            .padding(.top, 20)
            
            NavigationLink {
                ContentView()
                    .navigationBarBackButtonHidden()
            } label: {
                HStack {
                    Text("SIGN IN")
                        .bold()
                    Image(systemName: "arrow.right")
                }
                .foregroundStyle(.white)
                .frame(width: UIScreen.main.bounds.width - 32, height: 50)
            }
            .background(Color.blue)
            .cornerRadius(10)
            .padding(.top, 20)
            
            Spacer()
            
            // Sing up button
            
            NavigationLink {
                RegistrationView()
                    .navigationBarBackButtonHidden()
            } label: {
                HStack(spacing: 3) {
                    Text("Don`t have an account?")
                    Text("Sing up")
                        .bold()
                }
                .font(.system(size: 15))
            }
        }
    }
}

#Preview {
    LoginView()
}
