//
//  SettingView.swift
//  SafeLanding
//
//  Created by Data on 04.07.24.
//

import SwiftUI

struct SettingView: View {
    var body: some View {
        List {
            Section {
                HStack {
                    Text(User.MOCK_USER.initials)
                        .font(.title)
                        .bold()
                        .foregroundStyle(.white)
                        .frame(width: 70, height: 70)
                        .background(Color(.systemGray3))
                        .clipShape(Circle())
                    VStack(alignment: .leading) {
                        Text(User.MOCK_USER.fullName)
                            .font(.subheadline)
                            .bold()
                        Text(User.MOCK_USER.email)
                            .font(.footnote)
                            .foregroundStyle(.gray)
                    }
                }
            }
            
            Section("General") {
                HStack {
                    SettingRowView(imageName: "gear",
                                   title: "Version",
                                   tintColor: Color(.systemGray))
                    Spacer()
                    
                    Text("1.0.0")
                        .font(.subheadline)
                        .foregroundStyle(.gray)
                }
            }
            
            Section("Acclount") {
                Button(action: {
                    print("sss")
                }, label: {
                    SettingRowView(imageName: "arrow.left.circle.fill",
                                   title: "Sign Out",
                                   tintColor: .red)
                })
                
                Button(action: {
                    print("Delite account...")
                }, label: {
                    SettingRowView(imageName: "xmark.circle.fill",
                                   title: "Delite Account",
                                   tintColor: .red)
                })
            }
        }
    }
}

#Preview {
    SettingView()
}
