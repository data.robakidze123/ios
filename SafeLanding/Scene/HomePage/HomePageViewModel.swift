//
//  HomePageViewModel.swift
//  SafeLanding
//
//  Created by Data on 05.07.24.
//

import Foundation
import NetworkService

class HomePageViewModel: ObservableObject {
    @Published var businesses: [Business] = []
    private var networkService = NetworkService()
    
    func fetchBusinesses() {
        let urlString = "https://mocki.io/v1/e55f464f-d0d9-46fd-92fa-2bf1c425ebe1"
        networkService.getData(urlString: urlString) { (result: Result<[Business], Error>) in
            switch result {
            case .success(let businesses):
                self.businesses = businesses
            case .failure(let error):
                print("Error fetching data: \(error.localizedDescription)")
            }
        }
    }
}
