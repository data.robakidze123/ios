//
//  HomePage.swift
//  SafeLanding
//
//  Created by Data on 04.07.24.
//

import SwiftUI

struct HomePage: View {
    @StateObject var viewModel = HomePageViewModel()
    let gridItems = Array(repeating: GridItem(.flexible(), spacing: 10), count: 1)
    
    var body: some View {
        NavigationView {
            ScrollView {
                LazyVGrid(columns: gridItems, spacing: 5) {
                    ForEach(viewModel.businesses) { business in
                        EnterpriseCardView(business: business)
                    }
                }
            }
            .navigationTitle("Businesses")
            .onAppear {
                viewModel.fetchBusinesses()
            }
        }
    }
}

#Preview {
    HomePage()
}
