//
//  EnterpriseCardView.swift
//  SafeLanding
//
//  Created by Data on 05.07.24.
//

import SwiftUI

struct EnterpriseCardView: View {
    var business: Business
    
    @State private var image: Image? = nil
    
    var body: some View {
        ZStack {
            VStack(alignment: .leading) {
                if let image = image {
                    image
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(height: 150)
                        .clipped()
                } else {
                    Color.gray
                        .frame(height: 150)
                }
                
                Text(business.name)
                    .font(.title2)
                    .fontWeight(.bold)
                    .padding([.top, .leading, .trailing])
                
                Text(business.description)
                    .font(.subheadline)
                    .foregroundColor(.gray)
                    .padding([.leading, .trailing])
                
                HStack {
                    Text("$\(business.raised) raised of $\(business.goal)")
                        .font(.caption)
                        .foregroundColor(.gray)
                    Spacer()
                    Text("Deadline: \(business.deadline)")
                        .font(.caption)
                        .foregroundColor(.gray)
                }
                .padding([.leading, .trailing, .top])
                
                ProgressView(value: Double(business.raised) / Double(business.goal))
                    .padding([.leading, .trailing])

                NavigationLink(destination: DetailsView(business: business)) {
                    Text("VIEW DETAILS")
                        .font(.headline)
                        .frame(maxWidth: .infinity)
                        .padding()
                        .background(Color.blue)
                        .foregroundColor(.white)
                        .cornerRadius(8)
                }
                .padding()
            }
            .padding()
            .background(Color.white)
            .cornerRadius(10)
            .shadow(radius: 5)
            .padding()
        }
        .onAppear {
            loadImage()
        }
    }
    
    private func loadImage() {
        guard let url = URL(string: business.image) else { return }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let data = data, let uiImage = UIImage(data: data) {
                DispatchQueue.main.async {
                    self.image = Image(uiImage: uiImage)
                }
            }
        }.resume()
    }
}


//                NavigationLink(destination: EnterpriseDetailView(business: business))
