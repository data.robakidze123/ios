//
//  ProfileView.swift
//  SafeLanding
//
//  Created by Data on 04.07.24.
//

import SwiftUI

struct ProfileView: View {
    @State private var businesses: [Int] = Array(1...3)
    
    var body: some View {
        ScrollView {
            HStack {
                Image("personImage")
                    .resizable()
                    .scaledToFill()
                    .frame(width: 70, height: 70)
                    .clipShape(Circle())
                VStack(alignment: .leading) {
                    Text(User.MOCK_USER.fullName)
                        .font(.subheadline)
                        .bold()
                    Text(User.MOCK_USER.email)
                        .font(.footnote)
                        .foregroundStyle(.gray)
                    Text("555-245-335")
                        .font(.footnote)
                        .foregroundStyle(.gray)
                }
                Spacer()
            }
            .padding()
            HStack {
                Text("My Businesses:")
                    .bold()
                    .padding([.leading, .top])
                    .padding(.bottom, -10)
                Spacer()
            }
            ScrollView(.horizontal, showsIndicators: false) {
                HStack {
                    
                    ForEach(1..<4) { index in
                        MyBusinessesCardView()
                            .frame(width: 330)
                            .padding(.trailing, -20)
                    }
                }
                .padding(.trailing, -20)
            }
            Button(action: {
                addNewBusiness()
            }) {
                HStack {
                    Image(systemName: "plus")
                    Text("Add New Business")
                }
                .font(.headline)
                .frame(maxWidth: .infinity)
                .padding()
                .background(Color.blue)
                .foregroundColor(.white)
                .cornerRadius(8)
                .padding()
            }
            
            HStack {
                Text("Investments:")
                    .bold()
                    .padding()
                Spacer()
            }
            HStack {
                Text("Charities:")
                    .bold()
                    .padding()
                Spacer()
            }
            
        }
    }
    private func addNewBusiness() {
        businesses.append(businesses.count + 1)
    }
}

#Preview {
    ProfileView()
}
