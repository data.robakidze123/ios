//
//  MyBusinessesCardView.swift
//  SafeLanding
//
//  Created by Data on 05.07.24.
//

import SwiftUI

struct MyBusinessesCardView: View {
    var body: some View {
        VStack(alignment: .leading) {
            Image(.projectPhoto)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(height: 150)
                .clipped()
            
            Text("Doe Enterprises")
                .font(.title2)
                .fontWeight(.bold)
                .padding([.top, .leading, .trailing])
            
            Text("Innovative Solutions")
                .font(.subheadline)
                .foregroundColor(.gray)
                .padding([.leading, .trailing])
            
            Text("We provide innovative tech solutions.")
                .font(.body)
                .padding([.leading, .trailing])
            
            HStack {
                Text("$10000 raised of $50000")
                    .font(.caption)
                    .foregroundColor(.gray)
                Spacer()
                Text("Deadline: 1/1/2025")
                    .font(.caption)
                    .foregroundColor(.gray)
            }
            .padding()
            
        }
        .background(Color.white)
        .cornerRadius(10)
        .shadow(radius: 5)
        .padding()
        
    }
}

#Preview {
    MyBusinessesCardView()
}
