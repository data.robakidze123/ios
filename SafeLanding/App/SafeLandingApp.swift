//
//  SafeLandingApp.swift
//  SafeLanding
//
//  Created by Data on 03.07.24.
//

import SwiftUI

@main
struct SafeLandingApp: App {
    var body: some Scene {
        WindowGroup {
            LoginView()
        }
    }
}
